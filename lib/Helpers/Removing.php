<?php
/**
 * @package   Hedera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.03.02
 * @link      https://fabrika-klientov.ua
 * */

namespace Hedera\Helpers;

use GraphAware\Neo4j\OGM\Common\Collection as HederaCollection;

trait Removing
{
    /**
     * @param array $ids
     * @param bool $detachRelationships
     * @return bool
     */
    public function removeForIds(array $ids, bool $detachRelationships = false)
    {
        if (empty($ids)) {
            return true;
        }

        $ids = new HederaCollection(array_values($ids));

        if (!$ids->forAll(
            function ($key, $item) {
                return is_int($item);
            }
        )) {
            return false;
        }

        $graph = last(explode('\\', $this->classMetadata->getLabel()));

        $cql = 'MATCH (n:' . $graph . ') WHERE ID(n) IN $id ' . ($detachRelationships ? 'DETACH ' : '') . 'DELETE n';

        $query = $this->entityManager->createQuery($cql);
        $query->setParameter('id', $ids->getValues());

        $query->execute();

        return true;
    }
}
