<?php
/**
 * @package   Hedera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.12.25
 * @link      https://fabrika-klientov.ua
 * */

namespace Hedera\Repositories\Google;

use GraphAware\Neo4j\OGM\Repository\BaseRepository;
use Hedera\Helpers\Removing;
use Hedera\Helpers\WithBuilder;

class VioletFilesRepository extends BaseRepository
{
    use WithBuilder;
    use Removing;
}
