<?php
/**
 * @package   Hedera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.19
 * @link      https://fabrika-klientov.ua
 * */

namespace Hedera\Repositories\Magenta;

use GraphAware\Neo4j\OGM\Repository\BaseRepository;

class MagentaAccountsRepository extends BaseRepository
{

}
