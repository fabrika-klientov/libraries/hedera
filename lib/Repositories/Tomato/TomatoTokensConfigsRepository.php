<?php
/**
 * @package   Hedera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.07.07
 * @link      https://fabrika-klientov.ua
 * */

namespace Hedera\Repositories\Tomato;

use GraphAware\Neo4j\OGM\Common\Collection as HederaCollection;
use GraphAware\Neo4j\OGM\Repository\BaseRepository;
use Hedera\Helpers\Removing;
use Hedera\Models\Tomato\TomatoInternetDocuments;

class TomatoTokensConfigsRepository extends BaseRepository
{
    use Removing;

    /**
     * @param array $tokenIds
     * @return HederaCollection
     */
    public function loadIDIdsForTokenIds(array $tokenIds)
    {
        $graph = last(explode('\\', $this->getClassName()));
        $idGraph = last(explode('\\', TomatoInternetDocuments::class));

        $cql = 'MATCH (n:' . $graph . ')-[:TOMATO_TOKEN_ID_IN]-(d:' . $idGraph . ') WHERE ID(n) IN $id RETURN ID(d)';

        $query = $this->entityManager->createQuery($cql);
        $query->setParameter('id', $tokenIds);

        $result = $query->getResult();

        return new HederaCollection(
            array_map(
                function ($item) {
                    return $item['ID(d)'];
                },
                $result
            )
        );
    }
}
