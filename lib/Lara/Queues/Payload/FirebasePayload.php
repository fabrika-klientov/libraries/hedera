<?php
/**
 * @package   Hedera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.05
 * @link      https://fabrika-klientov.ua
 * */

namespace Hedera\Lara\Queues\Payload;

use Hedera\Lara\Queues\PayloadService;
use Hedera\Services\FirebaseService;
use Hedera\Services\FirebaseSmartService;

class FirebasePayload extends BasePayload implements InjectPayload
{
    /**
     * @param FirebaseService $payload
     * */
    public function inject($payload)
    {
        $smartService = $payload->getSmartService();
        if (!$smartService->isReady()) {
            return;
        }

        $data = self::getData($smartService);

        self::addCallback(function ($connection, $queue, $payload) use ($data) {
            $jobData = $payload['data'];
            $jobData[PayloadService::KEY_METADATA] = $data;

            return ['data' => $jobData, PayloadService::KEY_METADATA => $data];
        });
    }

    /**
     * @param FirebaseService $payload
     * */
    public function injectLazy($payload)
    {
        $smartService = $payload->getSmartService();

        self::addCallback(function ($connection, $queue, $payload) use ($smartService) {
            $jobData = $payload['data'];

            if ($smartService->isReady()) {
                $metadata = self::getData($smartService);
                $jobData[PayloadService::KEY_METADATA] = $metadata;
            }

            return ['data' => $jobData, PayloadService::KEY_METADATA => $metadata ?? null];
        });
    }

    protected function getData(FirebaseSmartService $smartService): array
    {
        return array_merge(
            $this->payloadService->getStaticallyPayload(),
            [
                PayloadService::KEY_SHARED_USER_ID => $smartService->getSharedUsers()->getId(),
            ]
        );
    }
}
