<?php
/**
 * @package   Hedera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.06
 * @link      https://fabrika-klientov.ua
 * */

namespace Hedera\Lara\Queues\Payload;

use Hedera\Lara\Queues\PayloadService;

class FreePayload extends BasePayload implements InjectPayload
{
    /**
     * @param array $payload
     * */
    public function inject($payload)
    {
        $free = $payload;
        $data = self::getData($free);

        self::addCallback(function ($connection, $queue, $payload) use ($data) {
            $jobData = $payload['data'];
            $jobData[PayloadService::KEY_METADATA] = $data;

            return ['data' => $jobData, PayloadService::KEY_METADATA => $data];
        });
    }

    /**
     * @param array $payload
     * */
    public function injectLazy($payload)
    {
        $free = $payload;

        self::addCallback(function ($connection, $queue, $payload) use ($free) {
            $jobData = $payload['data'];

            $metadata = self::getData($free);
            $jobData[PayloadService::KEY_METADATA] = $metadata;

            return ['data' => $jobData, PayloadService::KEY_METADATA => $metadata];
        });
    }

    protected function getData(array $data): array
    {
        return array_merge(
            $this->payloadService->getStaticallyPayload(),
            $data
        );
    }
}
