<?php
/**
 * @package   Hedera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.09.17
 * @link      https://fabrika-klientov.ua
 * */

namespace Hedera\Lara\Queues\Payload;

use Hedera\Lara\Queues\PayloadService;
use Illuminate\Queue\Queue;

abstract class BasePayload
{
    protected $payloadService;

    public function __construct(PayloadService $payloadService)
    {
        $this->payloadService = $payloadService;
    }

    /**
     * @param \Closure $callback
     * */
    protected function addCallback($callback)
    {
        Queue::createPayloadUsing($callback);
    }
}
