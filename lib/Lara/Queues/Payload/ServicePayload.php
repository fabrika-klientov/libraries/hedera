<?php
/**
 * @package   Hedera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.09.17
 * @link      https://fabrika-klientov.ua
 * */

namespace Hedera\Lara\Queues\Payload;

use Hedera\Lara\Queues\PayloadService;
use Hedera\Models\SharedPeriods;
use Hedera\Services\GuardService;
use Hedera\Services\SmartService;

class ServicePayload extends BasePayload implements InjectPayload
{
    /**
     * @param GuardService $payload
     * */
    public function inject($payload)
    {
        $smartService = $payload->getSmartService();
        if (!$smartService->isReady()) {
            return;
        }

        $data = self::getData($smartService);

        self::addCallback(function ($connection, $queue, $payload) use ($data) {
            $jobData = $payload['data'];
            $jobData[PayloadService::KEY_METADATA] = $data;

            return ['data' => $jobData, PayloadService::KEY_METADATA => $data];
        });
    }

    /**
     * @param GuardService $payload
     * */
    public function injectLazy($payload)
    {
        $smartService = $payload->getSmartService();

        self::addCallback(function ($connection, $queue, $payload) use ($smartService) {
            $jobData = $payload['data'];

            if ($smartService->isReady()) {
                $metadata = self::getData($smartService);
                $jobData[PayloadService::KEY_METADATA] = self::getData($smartService);
            }

            return ['data' => $jobData, PayloadService::KEY_METADATA => $metadata ?? null];
        });
    }

    protected function getData(SmartService $smartService): array
    {
        return array_merge(
            $this->payloadService->getStaticallyPayload(),
            [
                PayloadService::KEY_SHARED_APIKEY_ID => $smartService->getSharedApiKey()->getId(),
                PayloadService::KEY_SHARED_CUSTOMERS_SERVICE_ID => $smartService->getSharedCustomersServices()->getId(),
                PayloadService::KEY_SHARED_CUSTOMERS_SERVICE_CODE => $smartService->getSharedCustomersServices()
                    ->getCode(),
                PayloadService::KEY_SHARED_PERIODS_ID => $smartService->getSharedPeriods()
                    ->map(
                        function (SharedPeriods $item) {
                            return $item->getId();
                        }
                    )
                    ->getValues(),
                PayloadService::KEY_SHARED_AMOCRM_ID => $smartService->getSharedAmocrm()->getId(),
                PayloadService::KEY_SHARED_OAUTH_ID => $smartService->getSharedOauth()->getId(),
                PayloadService::KEY_SHARED_INTEGRATION_ID => $smartService->getSharedIntegrations()->getId(),
                PayloadService::KEY_SHARED_CUSTOMER_ID => $smartService->getSharedCustomers()->getId(),
            ]
        );
    }
}
