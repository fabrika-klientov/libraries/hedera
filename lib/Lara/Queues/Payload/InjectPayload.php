<?php
/**
 * @package   Hedera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.09.17
 * @link      https://fabrika-klientov.ua
 * */

namespace Hedera\Lara\Queues\Payload;

interface InjectPayload
{
    /**
     * @param mixed $payload
     * */
    public function inject($payload);

    /**
     * @param mixed $payload
     * */
    public function injectLazy($payload);
}
