<?php
/**
 * @package   Hedera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.09.17
 * @link      https://fabrika-klientov.ua
 * */

namespace Hedera\Lara\Queues\Payload;

use Hedera\Services\GuardService;

class NullPayload extends BasePayload implements InjectPayload
{
    /**
     * @param GuardService $payload
     * */
    public function inject($payload)
    {
        //
    }

    /**
     * @param GuardService $payload
     * */
    public function injectLazy($payload)
    {
        //
    }
}
