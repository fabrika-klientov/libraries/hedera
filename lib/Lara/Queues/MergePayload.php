<?php
/**
 * @package   Hedera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.06
 * @link      https://fabrika-klientov.ua
 * */

namespace Hedera\Lara\Queues;

use Illuminate\Queue\Queue;

trait MergePayload
{
    /**
     * @param mixed ...$params
     */
    protected static function mergeToPayload(...$params)
    {
        if (empty($params)) {
            return;
        }

        $data = PayloadService::getPayloadDataFor($params);

        Queue::createPayloadUsing(function ($connection, $queue, $payload) use ($data) {
            $jobData = $payload['data'];

            $metadata = $jobData[PayloadService::KEY_METADATA] ?? [];
            $metadata = array_merge($metadata, $data);
            $jobData[PayloadService::KEY_METADATA] = $metadata;

            return ['data' => $jobData, PayloadService::KEY_METADATA => $metadata];
        });
    }
}
