<?php
/**
 * @package   Hedera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.09.17
 * @link      https://fabrika-klientov.ua
 * */

namespace Hedera\Lara\Queues\Listeners;

use Hedera\Lara\Queues\MergePayload;
use Hedera\Lara\Queues\QueuePayload;

abstract class BaseListener
{
    use QueuePayload;
    use MergePayload;
}
