<?php
/**
 * @package   Hedera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.09.17
 * @link      https://fabrika-klientov.ua
 * */

namespace Hedera\Lara\Queues;

use Hedera\Lara\Queues\Payload;
use Hedera\Models\SharedAmocrm;
use Hedera\Models\SharedApikeys;
use Hedera\Models\SharedCustomers;
use Hedera\Models\SharedCustomersServices;
use Hedera\Models\SharedIntegrations;
use Hedera\Models\SharedOauth;
use Hedera\Models\SharedPeriods;
use Hedera\Services\FirebaseService;
use Hedera\Services\GuardService;
use Illuminate\Queue\Queue;

class PayloadService
{
    public const KEY_METADATA = 'venus_metadata';

    public const KEY_BOX_TYPE = 'box_type'; // service (tomato, google), system (teal), ...
    public const KEY_BOX_CODE = 'box_code'; // tomato, google, ...
    public const KEY_SHARED_APIKEY_ID = 'apikey_id';
    public const KEY_SHARED_CUSTOMERS_SERVICE_ID = 'service_id';
    public const KEY_SHARED_CUSTOMERS_SERVICE_CODE = 'service_code';
    public const KEY_SHARED_PERIODS_ID = 'period_ids';
    public const KEY_SHARED_AMOCRM_ID = 'amocrm_id';
    public const KEY_SHARED_OAUTH_ID = 'amocrm_oauth_id';
    public const KEY_SHARED_INTEGRATION_ID = 'amocrm_integration_id';
    public const KEY_SHARED_CUSTOMER_ID = 'customer_id';

    public const KEY_SHARED_USER_ID = 'user_id';

    protected $staticallyPayload;
    protected $lazy;

    public function __construct(array $staticallyPayload = [], bool $lazy = false)
    {
        $this->staticallyPayload = $staticallyPayload;
        $this->lazy = $lazy;
    }

    /**
     * @param mixed $payload
     * */
    public function inject($payload)
    {
        switch (true) {
            case is_array($payload):
                $payloadInjector = new Payload\FreePayload($this);
                break;

            case $payload instanceof GuardService:
                $payloadInjector = new Payload\ServicePayload($this);
                break;

            case $payload instanceof FirebaseService:
                $payloadInjector = new Payload\FirebasePayload($this);
                break;

            default:
                $payloadInjector = new Payload\NullPayload($this);
        }

        if ($this->lazy) {
            $payloadInjector->injectLazy($payload);
        } else {
            $payloadInjector->inject($payload);
        }
    }

    /**
     * @return array
     */
    public function getStaticallyPayload(): array
    {
        return $this->staticallyPayload;
    }

    public static function getPayloadDataFor(...$params): array
    {
        if (empty($params)) {
            return [];
        }

        $result = [];
        foreach ($params as $param) {
            $additional = [];
            switch (true) {
                case is_array($param):
                    $additional = $param;
                    break;

                case $param instanceof SharedApikeys:
                    $additional = [self::KEY_SHARED_APIKEY_ID => $param->getId()];
                    break;

                case $param instanceof SharedCustomersServices:
                    $additional = [
                        self::KEY_SHARED_CUSTOMERS_SERVICE_ID => $param->getId(),
                        self::KEY_SHARED_CUSTOMERS_SERVICE_CODE => $param->getCode(),
                    ];
                    break;

                case $param instanceof SharedPeriods:
                    $additional = [
                        self::KEY_SHARED_PERIODS_ID => array_merge(
                            $result[self::KEY_SHARED_PERIODS_ID] ?? [],
                            [$param->getId()]
                        ),
                    ];
                    break;

                case $param instanceof SharedAmocrm:
                    $additional = [self::KEY_SHARED_AMOCRM_ID => $param->getId()];
                    break;

                case $param instanceof SharedOauth:
                    $additional = [self::KEY_SHARED_OAUTH_ID => $param->getId()];
                    break;

                case $param instanceof SharedIntegrations:
                    $additional = [self::KEY_SHARED_INTEGRATION_ID => $param->getId()];
                    break;

                case $param instanceof SharedCustomers:
                    $additional = [self::KEY_SHARED_CUSTOMER_ID => $param->getId()];
                    break;
            }

            $result = array_merge($result, $additional);
        }

        return $result;
    }

    /**
     * @return void
     */
    public static function clean()
    {
        Queue::createPayloadUsing(null);
    }
}
