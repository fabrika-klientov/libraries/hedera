<?php
/**
 * @package   Hedera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.06
 * @link      https://fabrika-klientov.ua
 * */

namespace Hedera\Lara\Queues;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\Queue;

trait QueuePayload
{
    use InteractsWithQueue;

    /**
     * @param array|null $with
     */
    protected function injectPayload(array $with = null)
    {
        if ($this->job && $this instanceof ShouldQueue) {
            $payload = $this->job->payload();
            $deep = $payload['data'][PayloadService::KEY_METADATA] ?? $payload[PayloadService::KEY_METADATA] ?? [];
            if (!empty($with)) {
                $deep = array_merge($deep, $with);
            }

            Queue::createPayloadUsing(function ($connection, $queue, $payload) use ($deep) {
                $jobData = $payload['data'];
                $jobData[PayloadService::KEY_METADATA] = $deep;

                return ['data' => $jobData, PayloadService::KEY_METADATA => $deep];
            });
        }
    }
}
