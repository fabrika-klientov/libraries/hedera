<?php
/**
 * @package   Hedera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.07.06
 * @link      https://fabrika-klientov.ua
 * */

namespace Hedera\Models;

use GraphAware\Neo4j\OGM\Annotations as OGM;
use Hedera\Helpers\EntityFactory;
use Hedera\Helpers\SerializationHelper;

/**
 * @OGM\Node(label="SharedPeriods", repository="Hedera\Repositories\SharedPeriodsRepository")
 */
class SharedPeriods implements \JsonSerializable
{
    use EntityFactory;
    use SerializationHelper;

    /**
     * @var int
     *
     * @OGM\GraphId()
     */
    protected $id;

    /**
     * @var string
     *
     * @OGM\Property(type="string", key="date_start")
     */
    protected $dateStart;

    /**
     * @var string
     *
     * @OGM\Property(type="string", key="date_end")
     */
    protected $dateEnd;

    /**
     * @var string
     *
     * @OGM\Property(type="string")
     */
    protected $period;

    /**
     * @var bool|null
     *
     * @OGM\Property(type="boolean", nullable=true)
     */
    protected $gift;

    /**
     * @var DirectoryPeriods|null
     *
     * @OGM\Relationship(type="PERIOD_DIR_IN", direction="OUTGOING", collection=false, mappedBy="sharedPeriods", targetEntity="DirectoryPeriods")
     */
    protected $directoryPeriods;

    /**
     * @var SharedCustomersServices|null
     *
     * @OGM\Relationship(type="PERIOD_IN", direction="OUTGOING", collection=false, mappedBy="sharedPeriods", targetEntity="SharedCustomersServices")
     */
    protected $sharedCustomersServices;

    /**
     * @var SharedCustomersServices|null
     *
     * @OGM\Relationship(type="SHARED_PERIODS_UNVERIFIED_SHARED_CUSTOMERS_SERVICE", direction="OUTGOING", collection=false, mappedBy="unverifiedSharedPeriods", targetEntity="SharedCustomersServices")
     */
    protected $unverifiedSharedCustomersServices;

    /**
     * @var SharedTransactions|null
     *
     * @OGM\Relationship(type="SHARED_PERIODS_TO_SHARED_TRANSACTIONS", direction="OUTGOING", collection=false, mappedBy="sharedPeriods", targetEntity="SharedTransactions")
     */
    protected $sharedTransactions;

    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDateStart(): string
    {
        return $this->dateStart;
    }

    /**
     * @param string $dateStart
     */
    public function setDateStart(string $dateStart): void
    {
        $this->dateStart = $dateStart;
    }

    /**
     * @return string
     */
    public function getDateEnd(): string
    {
        return $this->dateEnd;
    }

    /**
     * @param string $dateEnd
     */
    public function setDateEnd(string $dateEnd): void
    {
        $this->dateEnd = $dateEnd;
    }

    /**
     * @return string
     */
    public function getPeriod(): string
    {
        return $this->period;
    }

    /**
     * @param string $period
     */
    public function setPeriod(string $period): void
    {
        $this->period = $period;
    }

    /**
     * @return bool|null
     */
    public function isGift(): ?bool
    {
        return $this->gift;
    }

    /**
     * @param bool|null $gift
     */
    public function setGift(?bool $gift): void
    {
        $this->gift = $gift;
    }

    /**
     * @return DirectoryPeriods|null
     */
    public function getDirectoryPeriods(): ?DirectoryPeriods
    {
        return $this->directoryPeriods;
    }

    /**
     * @param DirectoryPeriods|null $directoryPeriods
     */
    public function setDirectoryPeriods(?DirectoryPeriods $directoryPeriods): void
    {
        $this->directoryPeriods = $directoryPeriods;
    }

    /**
     * @return SharedCustomersServices|null
     */
    public function getSharedCustomersServices(): ?SharedCustomersServices
    {
        return $this->sharedCustomersServices;
    }

    /**
     * @param SharedCustomersServices|null $sharedCustomerServices
     */
    public function setSharedCustomersServices(?SharedCustomersServices $sharedCustomerServices): void
    {
        $this->sharedCustomersServices = $sharedCustomerServices;
    }

    /**
     * @return SharedCustomersServices|null
     */
    public function getUnverifiedSharedCustomersServices(): ?SharedCustomersServices
    {
        return $this->unverifiedSharedCustomersServices;
    }

    /**
     * @param SharedCustomersServices|null $unverifiedSharedCustomersServices
     */
    public function setUnverifiedSharedCustomersServices(?SharedCustomersServices $unverifiedSharedCustomersServices
    ): void {
        $this->unverifiedSharedCustomersServices = $unverifiedSharedCustomersServices;
    }

    /**
     * @return SharedTransactions|null
     */
    public function getSharedTransactions(): ?SharedTransactions
    {
        return $this->sharedTransactions;
    }

    /**
     * @param SharedTransactions|null $sharedTransactions
     */
    public function setSharedTransactions(?SharedTransactions $sharedTransactions): void
    {
        $this->sharedTransactions = $sharedTransactions;
    }

    public function jsonSerialize()
    {
        return self::serializing();
    }
}
