<?php
/**
 * @package   Hedera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.19
 * @link      https://fabrika-klientov.ua
 * */

namespace Hedera\Models\Magenta;

use GraphAware\Neo4j\OGM\Annotations as OGM;
use Hedera\Helpers\EntityFactory;
use Hedera\Helpers\SerializationHelper;
use Hedera\Helpers\WithTimestamps;
use Hedera\Models\SharedCustomersServices;

/**
 * @deprecated
 * todo: is main ttn in lead, is edit ttn
 * @OGM\Node(label="MagentaLinkedDocuments", repository="Hedera\Repositories\Magenta\MagentaLinkedDocumentsRepository")
 */
class MagentaLinkedDocuments implements \JsonSerializable
{
    use EntityFactory;
    use SerializationHelper;
    use WithTimestamps;

    /**
     * @var int
     *
     * @OGM\GraphId()
     */
    protected $id;

    /**
     * @var int
     *
     * @OGM\Property(type="int")
     */
    protected $entityId;

    /**
     * @var string|null
     *
     * @OGM\Property(type="string", nullable=true)
     */
    protected $entityType;

    /**
     * @var string
     *
     * @OGM\Property(type="string")
     */
    protected $ttn;

    /**
     * @var string
     *
     * @OGM\Property(type="string")
     */
    protected $logisticsType;

    /**
     * @var string
     *
     * @OGM\Property(type="string")
     */
    protected $logisticsId;

    /**
     * @var string
     *
     * @OGM\Property(type="string")
     */
    protected $logisticsStatus;

    /**
     * @var bool|null
     *
     * @OGM\Property(type="boolean")
     */
    protected $revoked;

    /**
     * @var mixed
     *
     * @OGM\Property(type="array")
     * @OGM\Convert(type="nested")
     */
    protected $additional;

    /**
     * @var MagentaAccounts|null
     *
     * @OGM\Relationship(type="MAGENTA_LINKED_DOCUMENTS_TO_MAGENTA_ACCOUNTS", direction="OUTGOING", collection=false, mappedBy="magentaLinkedDocuments", targetEntity="MagentaAccounts")
     */
    protected $magentaAccounts;

    /**
     * @var SharedCustomersServices|null
     *
     * @OGM\Relationship(type="MAGENTA_LINKED_DOCUMENTS_TO_SHARED_CUSTOMERS_SERVICES", direction="OUTGOING", collection=false, targetEntity="Hedera\Models\SharedCustomersServices")
     */
    protected $sharedCustomersServices;

    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getEntityId(): int
    {
        return $this->entityId;
    }

    /**
     * @param int $entityId
     */
    public function setEntityId(int $entityId): void
    {
        $this->entityId = $entityId;
    }

    /**
     * @return string|null
     */
    public function getEntityType(): ?string
    {
        return $this->entityType;
    }

    /**
     * @param string|null $entityType
     */
    public function setEntityType(?string $entityType): void
    {
        $this->entityType = $entityType;
    }

    /**
     * @return string
     */
    public function getTtn(): string
    {
        return $this->ttn;
    }

    /**
     * @param string $ttn
     */
    public function setTtn(string $ttn): void
    {
        $this->ttn = $ttn;
    }

    /**
     * @return string
     */
    public function getLogisticsType(): string
    {
        return $this->logisticsType;
    }

    /**
     * @param string $logisticsType
     */
    public function setLogisticsType(string $logisticsType): void
    {
        $this->logisticsType = $logisticsType;
    }

    /**
     * @return string
     */
    public function getLogisticsId(): string
    {
        return $this->logisticsId;
    }

    /**
     * @param string $logisticsId
     */
    public function setLogisticsId(string $logisticsId): void
    {
        $this->logisticsId = $logisticsId;
    }

    /**
     * @return string
     */
    public function getLogisticsStatus(): string
    {
        return $this->logisticsStatus;
    }

    /**
     * @param string $logisticsStatus
     */
    public function setLogisticsStatus(string $logisticsStatus): void
    {
        $this->logisticsStatus = $logisticsStatus;
    }

    /**
     * @return bool|null
     */
    public function getRevoked(): ?bool
    {
        return $this->revoked;
    }

    /**
     * @param bool|null $revoked
     */
    public function setRevoked(?bool $revoked): void
    {
        $this->revoked = $revoked;
    }

    /**
     * @return mixed
     */
    public function getAdditional()
    {
        return $this->additional;
    }

    /**
     * @param mixed $additional
     */
    public function setAdditional($additional): void
    {
        $this->additional = $additional;
    }

    /**
     * @return MagentaAccounts|null
     */
    public function getMagentaAccounts(): ?MagentaAccounts
    {
        return $this->magentaAccounts;
    }

    /**
     * @param MagentaAccounts|null $magentaAccounts
     */
    public function setMagentaAccounts(?MagentaAccounts $magentaAccounts): void
    {
        $this->magentaAccounts = $magentaAccounts;
    }

    /**
     * @return SharedCustomersServices|null
     */
    public function getSharedCustomersServices(): ?SharedCustomersServices
    {
        return $this->sharedCustomersServices;
    }

    /**
     * @param SharedCustomersServices|null $sharedCustomersServices
     */
    public function setSharedCustomersServices(?SharedCustomersServices $sharedCustomersServices): void
    {
        $this->sharedCustomersServices = $sharedCustomersServices;
    }

    public function jsonSerialize()
    {
        return self::serializing();
    }
}
