<?php
/**
 * @package   Hedera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.19
 * @link      https://fabrika-klientov.ua
 * */

namespace Hedera\Models\Magenta;

use GraphAware\Neo4j\OGM\Annotations as OGM;
use Hedera\Models\SharedConfigs;

/**
 * @OGM\Node(label="SharedConfigs", repository="Hedera\Repositories\SharedConfigsRepository")
 */
class MagentaConfigs extends SharedConfigs
{
    /**
     * @var string|null
     *
     * @OGM\Property(type="string", nullable=true)
     */
    protected $logistics;

    /**
     * @var array|null
     *
     * @OGM\Property(type="array")
     * @OGM\Convert(type="nested")
     */
    protected $rules;

    /**
     * @var array|null
     *
     * @OGM\Property(type="array")
     * @OGM\Convert(type="nested")
     */
    protected $relationFields;

    /**
     * @var array|null
     *
     * @OGM\Property(type="array")
     * @OGM\Convert(type="nested")
     */
    protected $cities;

    /**
     * @var mixed
     *
     * @OGM\Property(type="array")
     * @OGM\Convert(type="nested")
     */
    protected $settings;

    /**
     * @return string|null
     */
    public function getLogistics(): ?string
    {
        return $this->logistics;
    }

    /**
     * @param string|null $logistics
     */
    public function setLogistics(?string $logistics): void
    {
        $this->logistics = $logistics;
    }

    /**
     * @return array|null
     */
    public function getRules(): ?array
    {
        return $this->rules;
    }

    /**
     * @param array|null $rules
     */
    public function setRules(?array $rules): void
    {
        $this->rules = $rules;
    }

    /**
     * @return array|null
     */
    public function getRelationFields(): ?array
    {
        return $this->relationFields;
    }

    /**
     * @param array|null $relationFields
     */
    public function setRelationFields(?array $relationFields): void
    {
        $this->relationFields = $relationFields;
    }

    /**
     * @return array|null
     */
    public function getCities(): ?array
    {
        return $this->cities;
    }

    /**
     * @param array|null $cities
     */
    public function setCities(?array $cities): void
    {
        $this->cities = $cities;
    }

    /**
     * @return mixed
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * @param mixed $settings
     */
    public function setSettings($settings): void
    {
        $this->settings = $settings;
    }
}
