<?php
/**
 * @package   Hedera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.19
 * @link      https://fabrika-klientov.ua
 * */

namespace Hedera\Models\Magenta;

use Doctrine\Common\Collections\Collection;
use GraphAware\Neo4j\OGM\Annotations as OGM;
use GraphAware\Neo4j\OGM\Common\Collection as HederaCollection;
use Hedera\Helpers\EntityFactory;
use Hedera\Helpers\SerializationHelper;
use Hedera\Helpers\WithTimestamps;
use Hedera\Models\SharedCustomersServices;

/**
 * @OGM\Node(label="MagentaAccounts", repository="Hedera\Repositories\Magenta\MagentaAccountsRepository")
 */
class MagentaAccounts implements \JsonSerializable
{
    use EntityFactory;
    use SerializationHelper;
    use WithTimestamps;

    /**
     * @var int
     *
     * @OGM\GraphId()
     */
    protected $id;

    /**
     * @var string
     *
     * @OGM\Property(type="string")
     */
    protected $name;

    /**
     * @var string
     *
     * @OGM\Property(type="string")
     */
    protected $type;

    /**
     * @var bool
     *
     * @OGM\Property(type="boolean")
     */
    protected $isTypeDefault;

    /**
     * @var bool
     *
     * @OGM\Property(type="boolean")
     */
    protected $isGlobalDefault;

    /**
     * @var mixed
     *
     * @OGM\Property(type="array")
     * @OGM\Convert(type="nested")
     */
    protected $settings;

    /**
     * @var mixed
     *
     * @OGM\Property(type="array")
     * @OGM\Convert(type="nested")
     */
    protected $configuration;

    /**
     * @var array|null
     *
     * @OGM\Property(type="array")
     * @OGM\Convert(type="nested")
     */
    protected $rules;

    /**
     * @var array|null
     *
     * @OGM\Property(type="array")
     * @OGM\Convert(type="nested")
     */
    protected $relationFields;

    /**
     * @var bool
     *
     * @OGM\Property(type="boolean")
     */
    protected $status;

    /**
     * @var bool
     *
     * @OGM\Property(type="boolean")
     */
    protected $active;

    /**
     * @var int
     *
     * @OGM\Property(type="int")
     */
    protected $number;

    /**
     * @deprecated
     * @var Collection
     *
     * @OGM\Relationship(type="MAGENTA_LINKED_DOCUMENTS_TO_MAGENTA_ACCOUNTS", direction="INCOMING", collection=true, mappedBy="magentaAccounts", targetEntity="MagentaLinkedDocuments")
     */
    protected $magentaLinkedDocuments;

    /**
     * @var SharedCustomersServices|null
     *
     * @OGM\Relationship(type="MAGENTA_ACCOUNTS_TO_SHARED_CUSTOMERS_SERVICES", direction="OUTGOING", collection=false, targetEntity="Hedera\Models\SharedCustomersServices")
     */
    protected $sharedCustomersServices;

    public function __construct()
    {
        $this->magentaLinkedDocuments = new HederaCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return bool
     */
    public function isTypeDefault(): bool
    {
        return $this->isTypeDefault;
    }

    /**
     * @param bool $isTypeDefault
     */
    public function setIsTypeDefault(bool $isTypeDefault): void
    {
        $this->isTypeDefault = $isTypeDefault;
    }

    /**
     * @return bool
     */
    public function isGlobalDefault(): bool
    {
        return $this->isGlobalDefault;
    }

    /**
     * @param bool $isGlobalDefault
     */
    public function setIsGlobalDefault(bool $isGlobalDefault): void
    {
        $this->isGlobalDefault = $isGlobalDefault;
    }

    /**
     * @return mixed
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * @param mixed $settings
     */
    public function setSettings($settings): void
    {
        $this->settings = $settings;
    }

    /**
     * @return mixed
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * @param mixed $configuration
     */
    public function setConfiguration($configuration): void
    {
        $this->configuration = $configuration;
    }

    /**
     * @return array|null
     */
    public function getRules(): ?array
    {
        return $this->rules;
    }

    /**
     * @param array|null $rules
     */
    public function setRules(?array $rules): void
    {
        $this->rules = $rules;
    }

    /**
     * @return array|null
     */
    public function getRelationFields(): ?array
    {
        return $this->relationFields;
    }

    /**
     * @param array|null $relationsFields
     */
    public function setRelationFields(?array $relationsFields): void
    {
        $this->relationFields = $relationsFields;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status): void
    {
        $this->status = $status;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return int
     */
    public function getNumber(): int
    {
        return $this->number;
    }

    /**
     * @param int $number
     */
    public function setNumber(int $number): void
    {
        $this->number = $number;
    }

    /**
     * @deprecated
     * @return Collection
     */
    public function getMagentaLinkedDocuments()
    {
        return $this->magentaLinkedDocuments;
    }

    /**
     * @deprecated
     * @param Collection $magentaLinkedDocuments
     */
    public function setMagentaLinkedDocuments(Collection $magentaLinkedDocuments): void
    {
        $this->magentaLinkedDocuments = $magentaLinkedDocuments;
    }

    /**
     * @return SharedCustomersServices|null
     */
    public function getSharedCustomersServices(): ?SharedCustomersServices
    {
        return $this->sharedCustomersServices;
    }

    /**
     * @param SharedCustomersServices|null $sharedCustomersServices
     */
    public function setSharedCustomersServices(?SharedCustomersServices $sharedCustomersServices): void
    {
        $this->sharedCustomersServices = $sharedCustomersServices;
    }

    public function jsonSerialize()
    {
        return self::serializing();
    }
}
