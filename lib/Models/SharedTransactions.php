<?php
/**
 * @package   Hedera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.06.29
 * @link      https://fabrika-klientov.ua
 * */

namespace Hedera\Models;

use Doctrine\Common\Collections\Collection;
use GraphAware\Neo4j\OGM\Annotations as OGM;
use GraphAware\Neo4j\OGM\Common\Collection as HederaCollection;
use Hedera\Helpers\EntityFactory;
use Hedera\Helpers\SerializationHelper;
use Hedera\Helpers\WithTimestamps;

/**
 * @OGM\Node(label="SharedTransactions", repository="Hedera\Repositories\SharedTransactionsRepository")
 */
class SharedTransactions implements \JsonSerializable
{
    use EntityFactory;
    use SerializationHelper;
    use WithTimestamps;

    /**
     * @var int
     *
     * @OGM\GraphId()
     */
    protected $id;

    /**
     * @var string|null
     *
     * @OGM\Property(type="string")
     */
    protected $paySystem;

    /**
     * @var string|null
     *
     * @OGM\Property(type="string")
     */
    protected $accountSystem;

    /**
     * @var bool
     *
     * @OGM\Property(type="boolean")
     */
    protected $successful;

    /**
     * @var string
     *
     * @OGM\Property(type="string")
     */
    protected $reference;

    /**
     * @var string|int|null
     *
     * @OGM\Property(type="string")
     */
    protected $amount;

    /**
     * @var string|null
     *
     * @OGM\Property(type="string")
     */
    protected $currency;

    /**
     * @var mixed|null
     *
     * @OGM\Property(type="array")
     * @OGM\Convert(type="nested")
     */
    protected $additional;

    /**
     * @var Collection
     *
     * @OGM\Relationship(type="SHARED_PERIODS_TO_SHARED_TRANSACTIONS", direction="INCOMING", collection=true, mappedBy="sharedTransactions", targetEntity="Hedera\Models\SharedPeriods")
     */
    protected $sharedPeriods;

    /**
     * @var Collection
     *
     * @OGM\Relationship(type="SHARED_TRANSACTIONS_TO_SHARED_AMOCRM", direction="OUTGOING", collection=true, mappedBy="sharedTransactions", targetEntity="Hedera\Models\SharedAmocrm")
     */
    protected $sharedAmocrm;

    /**
     * @var SharedCustomers|null
     *
     * @OGM\Relationship(type="SHARED_TRANSACTIONS_TO_SHARED_CUSTOMERS", direction="OUTGOING", collection=false, mappedBy="sharedTransactions", targetEntity="Hedera\Models\SharedCustomers")
     */
    protected $sharedCustomers;

    /**
     * @var SharedDiscounts|null
     *
     * @OGM\Relationship(type="SHARED_TRANSACTIONS_TO_SHARED_DISCOUNTS", direction="OUTGOING", collection=false, mappedBy="sharedTransactions", targetEntity="Hedera\Models\SharedDiscounts")
     */
    protected $sharedDiscounts;

    public function __construct()
    {
        $this->sharedPeriods = new HederaCollection();
        $this->sharedAmocrm = new HederaCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getPaySystem(): ?string
    {
        return $this->paySystem;
    }

    /**
     * @param string|null $paySystem
     */
    public function setPaySystem(?string $paySystem): void
    {
        $this->paySystem = $paySystem;
    }

    /**
     * @return string|null
     */
    public function getAccountSystem(): ?string
    {
        return $this->accountSystem;
    }

    /**
     * @param string|null $accountSystem
     */
    public function setAccountSystem(?string $accountSystem): void
    {
        $this->accountSystem = $accountSystem;
    }

    /**
     * @return bool
     */
    public function isSuccessful(): bool
    {
        return $this->successful;
    }

    /**
     * @param bool $successful
     */
    public function setSuccessful(bool $successful): void
    {
        $this->successful = $successful;
    }

    /**
     * @return string
     */
    public function getReference(): string
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     */
    public function setReference(string $reference): void
    {
        $this->reference = $reference;
    }

    /**
     * @return int|string|null
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int|string|null $amount
     */
    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string|null $currency
     */
    public function setCurrency(?string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed|null
     */
    public function getAdditional()
    {
        return $this->additional;
    }

    /**
     * @param mixed|null $additional
     */
    public function setAdditional($additional): void
    {
        $this->additional = $additional;
    }

    /**
     * @return Collection
     */
    public function getSharedPeriods()
    {
        return $this->sharedPeriods;
    }

    /**
     * @param Collection $sharedPeriods
     */
    public function setSharedPeriods($sharedPeriods): void
    {
        $this->sharedPeriods = $sharedPeriods;
    }

    /**
     * @return Collection
     */
    public function getSharedAmocrm()
    {
        return $this->sharedAmocrm;
    }

    /**
     * @param Collection $sharedAmocrm
     */
    public function setSharedAmocrm($sharedAmocrm): void
    {
        $this->sharedAmocrm = $sharedAmocrm;
    }

    /**
     * @return SharedCustomers|null
     */
    public function getSharedCustomers(): ?SharedCustomers
    {
        return $this->sharedCustomers;
    }

    /**
     * @param SharedCustomers|null $sharedCustomers
     */
    public function setSharedCustomers(?SharedCustomers $sharedCustomers): void
    {
        $this->sharedCustomers = $sharedCustomers;
    }

    /**
     * @return SharedDiscounts|null
     */
    public function getSharedDiscounts(): ?SharedDiscounts
    {
        return $this->sharedDiscounts;
    }

    /**
     * @param SharedDiscounts|null $sharedDiscounts
     */
    public function setSharedDiscounts(?SharedDiscounts $sharedDiscounts): void
    {
        $this->sharedDiscounts = $sharedDiscounts;
    }

    public function jsonSerialize()
    {
        return self::serializing();
    }
}
