<?php
/**
 * @package   Hedera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.07.28
 * @link      https://fabrika-klientov.ua
 * */

namespace Hedera\Models\Pang;

use Doctrine\Common\Collections\Collection;
use GraphAware\Neo4j\OGM\Annotations as OGM;
use GraphAware\Neo4j\OGM\Common\Collection as HederaCollection;
use Hedera\Helpers\EntityFactory;
use Hedera\Helpers\SerializationHelper;
use Hedera\Models\SharedCustomersServices;

/**
 * @OGM\Node(label="PangAccounts", repository="Hedera\Repositories\Pang\PangAccountsRepository")
 */
class PangAccounts implements \JsonSerializable
{
    use EntityFactory;
    use SerializationHelper;

    /**
     * @var int
     *
     * @OGM\GraphId()
     */
    protected $id;

    /**
     * @var string
     *
     * @OGM\Property(type="string")
     */
    protected $name;

    /**
     * @var string
     *
     * @OGM\Property(type="string")
     */
    protected $login;

    /**
     * @var string|null
     *
     * @OGM\Property(type="string", nullable=true)
     */
    protected $domainName;

    /**
     * @var string
     *
     * @OGM\Property(type="string")
     */
    protected $secretKey;

    /**
     * @var bool
     *
     * @OGM\Property(type="boolean")
     */
    protected $isDefault;

    /**
     * @var bool
     *
     * @OGM\Property(type="boolean")
     */
    protected $status;

    /**
     * @var bool
     *
     * @OGM\Property(type="boolean")
     */
    protected $active;

    /**
     * @var int
     *
     * @OGM\Property(type="int")
     */
    protected $number;

    /**
     * @var Collection
     *
     * @OGM\Relationship(type="PANG_LINKED_INVOICES_TO_PANG_ACCOUNTS", direction="INCOMING", collection=true, mappedBy="pangAccounts", targetEntity="PangLinkedInvoices")
     */
    protected $pangLinkedInvoices;

    /**
     * @var SharedCustomersServices|null
     *
     * @OGM\Relationship(type="PANG_ACCOUNTS_TO_SHARED_CUSTOMERS_SERVICES", direction="OUTGOING", collection=false, targetEntity="Hedera\Models\SharedCustomersServices")
     */
    protected $sharedCustomersServices;

    public function __construct()
    {
        $this->pangLinkedInvoices = new HederaCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    /**
     * @return string|null
     */
    public function getDomainName(): ?string
    {
        return $this->domainName;
    }

    /**
     * @param string|null $domainName
     */
    public function setDomainName(?string $domainName): void
    {
        $this->domainName = $domainName;
    }

    /**
     * @return string
     */
    public function getSecretKey(): string
    {
        return $this->secretKey;
    }

    /**
     * @param string $secretKey
     */
    public function setSecretKey(string $secretKey): void
    {
        $this->secretKey = $secretKey;
    }

    /**
     * @return bool
     */
    public function isDefault(): bool
    {
        return $this->isDefault;
    }

    /**
     * @param bool $isDefault
     */
    public function setIsDefault(bool $isDefault): void
    {
        $this->isDefault = $isDefault;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status): void
    {
        $this->status = $status;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return int
     */
    public function getNumber(): int
    {
        return $this->number;
    }

    /**
     * @param int $number
     */
    public function setNumber(int $number): void
    {
        $this->number = $number;
    }

    /**
     * @return Collection
     */
    public function getPangLinkedInvoices(): Collection
    {
        return $this->pangLinkedInvoices;
    }

    /**
     * @param Collection $pangLinkedInvoices
     */
    public function setPangLinkedInvoices(Collection $pangLinkedInvoices): void
    {
        $this->pangLinkedInvoices = $pangLinkedInvoices;
    }

    /**
     * @return SharedCustomersServices|null
     */
    public function getSharedCustomersServices(): ?SharedCustomersServices
    {
        return $this->sharedCustomersServices;
    }

    /**
     * @param SharedCustomersServices|null $sharedCustomersServices
     */
    public function setSharedCustomersServices(?SharedCustomersServices $sharedCustomersServices): void
    {
        $this->sharedCustomersServices = $sharedCustomersServices;
    }

    public function jsonSerialize()
    {
        return self::serializing();
    }
}
