<?php
/**
 * @package   Hedera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.07.28
 * @link      https://fabrika-klientov.ua
 * */

namespace Hedera\Models\Pang;

use GraphAware\Neo4j\OGM\Annotations as OGM;
use Hedera\Helpers\EntityFactory;
use Hedera\Helpers\SerializationHelper;
use Hedera\Helpers\WithTimestamps;
use Hedera\Models\SharedCustomersServices;

/**
 * @OGM\Node(label="PangLinkedInvoices", repository="Hedera\Repositories\Pang\PangLinkedInvoicesRepository")
 */
class PangLinkedInvoices implements \JsonSerializable
{
    use EntityFactory;
    use SerializationHelper;
    use WithTimestamps;

    /**
     * @var int
     *
     * @OGM\GraphId()
     */
    protected $id;

    /**
     * @var int
     *
     * @OGM\Property(type="int")
     */
    protected $entityId;

    /**
     * @var string|null
     *
     * @OGM\Property(type="string", nullable=true)
     */
    protected $entityType;

    /**
     * @var string
     *
     * @OGM\Property(type="string")
     */
    protected $orderReference;

    /**
     * @var string
     *
     * @OGM\Property(type="string")
     */
    protected $transactionStatus;

    /**
     * @var string|null
     *
     * @OGM\Property(type="string")
     */
    protected $expiredAt;

    /**
     * @var mixed
     *
     * @OGM\Property(type="array")
     * @OGM\Convert(type="nested")
     */
    protected $additional;

    /**
     * @var PangAccounts|null
     *
     * @OGM\Relationship(type="PANG_LINKED_INVOICES_TO_PANG_ACCOUNTS", direction="OUTGOING", collection=false, mappedBy="pangLinkedInvoices", targetEntity="PangAccounts")
     */
    protected $pangAccounts;

    /**
     * @var SharedCustomersServices|null
     *
     * @OGM\Relationship(type="PANG_LINKED_INVOICES_TO_SHARED_CUSTOMERS_SERVICES", direction="OUTGOING", collection=false, targetEntity="Hedera\Models\SharedCustomersServices")
     */
    protected $sharedCustomersServices;

    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getEntityId(): int
    {
        return $this->entityId;
    }

    /**
     * @param int $entityId
     */
    public function setEntityId(int $entityId): void
    {
        $this->entityId = $entityId;
    }

    /**
     * @return string|null
     */
    public function getEntityType(): ?string
    {
        return $this->entityType;
    }

    /**
     * @param string|null $entityType
     */
    public function setEntityType(?string $entityType): void
    {
        $this->entityType = $entityType;
    }

    /**
     * @return string
     */
    public function getOrderReference(): string
    {
        return $this->orderReference;
    }

    /**
     * @param string $orderReference
     */
    public function setOrderReference(string $orderReference): void
    {
        $this->orderReference = $orderReference;
    }

    /**
     * @return string
     */
    public function getTransactionStatus(): string
    {
        return $this->transactionStatus;
    }

    /**
     * @param string $transactionStatus
     */
    public function setTransactionStatus(string $transactionStatus): void
    {
        $this->transactionStatus = $transactionStatus;
    }

    /**
     * @return string|null
     */
    public function getExpiredAt(): ?string
    {
        return $this->expiredAt;
    }

    /**
     * @param string|null $expiredAt
     */
    public function setExpiredAt(?string $expiredAt): void
    {
        $this->expiredAt = $expiredAt;
    }

    /**
     * @return mixed
     */
    public function getAdditional()
    {
        return $this->additional;
    }

    /**
     * @param mixed $additional
     */
    public function setAdditional($additional): void
    {
        $this->additional = $additional;
    }

    /**
     * @return PangAccounts|null
     */
    public function getPangAccounts(): ?PangAccounts
    {
        return $this->pangAccounts;
    }

    /**
     * @param PangAccounts|null $pangAccounts
     */
    public function setPangAccounts(?PangAccounts $pangAccounts): void
    {
        $this->pangAccounts = $pangAccounts;
    }

    /**
     * @return SharedCustomersServices|null
     */
    public function getSharedCustomersServices(): ?SharedCustomersServices
    {
        return $this->sharedCustomersServices;
    }

    /**
     * @param SharedCustomersServices|null $sharedCustomersServices
     */
    public function setSharedCustomersServices(?SharedCustomersServices $sharedCustomersServices): void
    {
        $this->sharedCustomersServices = $sharedCustomersServices;
    }

    public function jsonSerialize()
    {
        return self::serializing();
    }
}
