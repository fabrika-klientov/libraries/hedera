<?php
/**
 * @package   Hedera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.07.20
 * @link      https://fabrika-klientov.ua
 * */

namespace Hedera\Models\SharedConfigs;

use GraphAware\Neo4j\OGM\Annotations as OGM;
use Hedera\Models\SharedConfigs;

/**
 * @OGM\Node(label="SharedConfigs", repository="Hedera\Repositories\SharedConfigsRepository")
 */
class NavyConfigs extends SharedConfigs
{
    /**
     * @var string|null
     *
     * @OGM\Property(type="string")
     */
    protected $typeNavy;

    /**
     * @var bool|null
     *
     * @OGM\Property(type="boolean")
     */
    protected $power;

    /**
     * @var mixed|null
     *
     * @OGM\Property(type="array")
     * @OGM\Convert(type="nested")
     */
    protected $users;

    /**
     * @var mixed|null
     *
     * @OGM\Property(type="array")
     * @OGM\Convert(type="nested")
     */
    protected $config;


    /**
     * @return string|null
     */
    public function getTypeNavy(): ?string
    {
        return $this->typeNavy;
    }

    /**
     * @param string|null $typeNavy
     */
    public function setTypeNavy(?string $typeNavy): void
    {
        $this->typeNavy = $typeNavy;
    }

    /**
     * @return bool|null
     */
    public function getPower(): ?bool
    {
        return $this->power;
    }

    /**
     * @param bool|null $power
     */
    public function setPower(?bool $power): void
    {
        $this->power = $power;
    }

    /**
     * @return mixed|null
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param mixed|null $users
     */
    public function setUsers($users): void
    {
        $this->users = $users;
    }

    /**
     * @return mixed|null
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param mixed|null $config
     */
    public function setConfig($config): void
    {
        $this->config = $config;
    }
}
