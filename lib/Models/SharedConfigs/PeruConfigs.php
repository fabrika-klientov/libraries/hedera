<?php
/**
 * @package   Hedera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.12.08
 * @link      https://fabrika-klientov.ua
 * */

namespace Hedera\Models\SharedConfigs;

use GraphAware\Neo4j\OGM\Annotations as OGM;
use Hedera\Models\SharedConfigs;

/**
 * @OGM\Node(label="SharedConfigs", repository="Hedera\Repositories\SharedConfigsRepository")
 */
class PeruConfigs extends SharedConfigs
{
    /**
     * @var string|null
     *
     * @OGM\Property(type="string", nullable=true)
     */
    protected $typeConfig;

    /**
     * @var mixed
     *
     * @OGM\Property(type="array")
     * @OGM\Convert(type="nested")
     */
    protected $settings;

    /**
     * @return string|null
     */
    public function getTypeConfig(): ?string
    {
        return $this->typeConfig;
    }

    /**
     * @param string|null $typeConfig
     */
    public function setTypeConfig(?string $typeConfig): void
    {
        $this->typeConfig = $typeConfig;
    }

    /**
     * @return mixed
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * @param mixed $settings
     */
    public function setSettings($settings): void
    {
        $this->settings = $settings;
    }
}
