<?php
/**
 * @package   Hedera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.07.05
 * @link      https://fabrika-klientov.ua
 * */

namespace Hedera\Models;

use Doctrine\Common\Collections\Collection;
use GraphAware\Neo4j\OGM\Annotations as OGM;
use GraphAware\Neo4j\OGM\Common\Collection as HederaCollection;
use Hedera\Helpers\EntityFactory;
use Hedera\Helpers\SerializationHelper;
use Hedera\Helpers\WithTimestamps;

/**
 * @OGM\Node(label="SharedDiscounts", repository="Hedera\Repositories\SharedDiscountsRepository")
 */
class SharedDiscounts implements \JsonSerializable
{
    use EntityFactory;
    use SerializationHelper;
    use WithTimestamps;

    /**
     * @var int
     *
     * @OGM\GraphId()
     */
    protected $id;

    /**
     * Name of Discount (free property)
     * @var string
     *
     * @OGM\Property(type="string")
     */
    protected $name;

    /**
     * Title Name of Discount
     * @var string
     *
     * @OGM\Property(type="string")
     */
    protected $title;

    /**
     * Description of Discount
     * @var string|null
     *
     * @OGM\Property(type="string", nullable=true)
     */
    protected $description;

    /**
     * Main Type of Discount (widget|amocrm|free|etc...)
     * @var string
     *
     * @OGM\Property(type="string")
     */
    protected $type;

    /**
     * Type of Discount (percent|diff|etc...)
     * @var string
     *
     * @OGM\Property(type="string")
     */
    protected $typeDiscount;

    /**
     * Value of Discount (20% for percent|100 for diff|...) if more currencies -> set additional
     * @var string|int
     *
     * @OGM\Property(type="string")
     */
    protected $discount;

    /**
     * Max value of Discount (optional) (200)
     * @var string|int|null
     *
     * @OGM\Property(type="string", nullable=true)
     */
    protected $maxDiscount;

    /**
     * Round off Discount (optional) (-10|-5|-1|0|+1|+5|+10) 0 - do not round
     * @var string|int|null
     *
     * @OGM\Property(type="string", nullable=true)
     */
    protected $round;

    /**
     * Once per (service|amocrm|customer|...) Discount (optional)
     * @var bool|null
     *
     * @OGM\Property(type="boolean", nullable=true)
     */
    protected $oncePer;

    /**
     * Has promo of Discount (optional)
     * @var bool|null
     *
     * @OGM\Property(type="boolean", nullable=true)
     */
    protected $promo;

    /**
     * Promo code of Discount (optional)
     * @var string|null
     *
     * @OGM\Property(type="string", nullable=true)
     */
    protected $promoCode;

    /**
     * Has owner of Discount (optional) (SharedCustomersServices|Amocrm|Customer|etc...) see relations between _OWNED_
     * @var bool|null
     *
     * @OGM\Property(type="boolean", nullable=true)
     */
    protected $owned;

    /**
     * Start date of Discount (optional)
     * @var string|null
     *
     * @OGM\Property(type="string", nullable=true)
     */
    protected $start;

    /**
     * End date of Discount (optional)
     * @var string|null
     *
     * @OGM\Property(type="string", nullable=true)
     */
    protected $expired;

    /**
     * Available Count for using this Discount
     * @var int|null
     *
     * @OGM\Property(type="int", nullable=true)
     */
    protected $availableCount;

    /**
     * All Count used this Discount (with unsuccessful)
     * @var int|null
     *
     * @OGM\Property(type="int", nullable=true)
     */
    protected $count;

    /**
     * Successful Count used this Discount
     * @var int|null
     *
     * @OGM\Property(type="int", nullable=true)
     */
    protected $successCount;

    /**
     * Force revoked this Discount
     * @var bool|null
     *
     * @OGM\Property(type="boolean", nullable=true)
     */
    protected $revoked;

    /**
     * Deep data of Discount (optional)
     * @var mixed|null
     *
     * @OGM\Property(type="array")
     * @OGM\Convert(type="nested")
     */
    protected $additional;

    /**
     * @var Collection
     *
     * @OGM\Relationship(type="SHARED_TRANSACTIONS_TO_SHARED_DISCOUNTS", direction="INCOMING", collection=true, mappedBy="sharedDiscounts", targetEntity="Hedera\Models\SharedTransactions")
     */
    protected $sharedTransactions;

    /**
     * @var Collection
     *
     * @OGM\Relationship(type="SHARED_WIDGETS_TO_SHARED_DISCOUNTS", direction="INCOMING", collection=true, mappedBy="sharedDiscounts", targetEntity="Hedera\Models\SharedWidgets")
     */
    protected $sharedWidgets;

    /**
     * @var SharedCustomersServices|null
     *
     * @OGM\Relationship(type="SHARED_DISCOUNTS_OWNED_SHARED_CUSTOMERS_SERVICES", direction="OUTGOING", collection=false, mappedBy="sharedDiscounts", targetEntity="Hedera\Models\SharedCustomersServices")
     */
    protected $sharedCustomersServices;

    /**
     * @var SharedAmocrm|null
     *
     * @OGM\Relationship(type="SHARED_DISCOUNTS_OWNED_SHARED_AMOCRM", direction="OUTGOING", collection=false, mappedBy="sharedDiscounts", targetEntity="Hedera\Models\SharedAmocrm")
     */
    protected $sharedAmocrm;

    /**
     * @var SharedCustomers|null
     *
     * @OGM\Relationship(type="SHARED_DISCOUNTS_OWNED_SHARED_CUSTOMERS", direction="OUTGOING", collection=false, mappedBy="sharedDiscounts", targetEntity="Hedera\Models\SharedCustomers")
     */
    protected $sharedCustomers;

    public function __construct()
    {
        $this->sharedTransactions = new HederaCollection();
        $this->sharedWidgets = new HederaCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getTypeDiscount(): string
    {
        return $this->typeDiscount;
    }

    /**
     * @param string $typeDiscount
     */
    public function setTypeDiscount(string $typeDiscount): void
    {
        $this->typeDiscount = $typeDiscount;
    }

    /**
     * @return int|string
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param int|string $discount
     */
    public function setDiscount($discount): void
    {
        $this->discount = $discount;
    }

    /**
     * @return int|string|null
     */
    public function getMaxDiscount()
    {
        return $this->maxDiscount;
    }

    /**
     * @param int|string|null $maxDiscount
     */
    public function setMaxDiscount($maxDiscount): void
    {
        $this->maxDiscount = $maxDiscount;
    }

    /**
     * @return int|string|null
     */
    public function getRound()
    {
        return $this->round;
    }

    /**
     * @param int|string|null $round
     */
    public function setRound($round): void
    {
        $this->round = $round;
    }

    /**
     * @return bool|null
     */
    public function getOncePer(): ?bool
    {
        return $this->oncePer;
    }

    /**
     * @param bool|null $oncePer
     */
    public function setOncePer(?bool $oncePer): void
    {
        $this->oncePer = $oncePer;
    }

    /**
     * @return bool|null
     */
    public function getPromo(): ?bool
    {
        return $this->promo;
    }

    /**
     * @param bool|null $promo
     */
    public function setPromo(?bool $promo): void
    {
        $this->promo = $promo;
    }

    /**
     * @return string|null
     */
    public function getPromoCode(): ?string
    {
        return $this->promoCode;
    }

    /**
     * @param string|null $promoCode
     */
    public function setPromoCode(?string $promoCode): void
    {
        $this->promoCode = $promoCode;
    }

    /**
     * @return bool|null
     */
    public function getOwned(): ?bool
    {
        return $this->owned;
    }

    /**
     * @param bool|null $owned
     */
    public function setOwned(?bool $owned): void
    {
        $this->owned = $owned;
    }

    /**
     * @return string|null
     */
    public function getStart(): ?string
    {
        return $this->start;
    }

    /**
     * @param string|null $start
     */
    public function setStart(?string $start): void
    {
        $this->start = $start;
    }

    /**
     * @return string|null
     */
    public function getExpired(): ?string
    {
        return $this->expired;
    }

    /**
     * @param string|null $expired
     */
    public function setExpired(?string $expired): void
    {
        $this->expired = $expired;
    }

    /**
     * @return string|null
     */
    public function getAvailableCount(): ?string
    {
        return $this->availableCount;
    }

    /**
     * @param int|null $availableCount
     */
    public function setAvailableCount(?int $availableCount): void
    {
        $this->availableCount = $availableCount;
    }

    /**
     * @return int|null
     */
    public function getCount(): ?int
    {
        return $this->count;
    }

    /**
     * @param int|null $count
     */
    public function setCount(?int $count): void
    {
        $this->count = $count;
    }

    /**
     * @return int|null
     */
    public function getSuccessCount(): ?int
    {
        return $this->successCount;
    }

    /**
     * @param int|null $successCount
     */
    public function setSuccessCount(?int $successCount): void
    {
        $this->successCount = $successCount;
    }

    /**
     * @return bool|null
     */
    public function getRevoked(): ?bool
    {
        return $this->revoked;
    }

    /**
     * @param bool|null $revoked
     */
    public function setRevoked(?bool $revoked): void
    {
        $this->revoked = $revoked;
    }

    /**
     * @return mixed|null
     */
    public function getAdditional()
    {
        return $this->additional;
    }

    /**
     * @param mixed|null $additional
     */
    public function setAdditional($additional): void
    {
        $this->additional = $additional;
    }

    /**
     * @return Collection
     */
    public function getSharedTransactions()
    {
        return $this->sharedTransactions;
    }

    /**
     * @param Collection $sharedTransactions
     */
    public function setSharedTransactions($sharedTransactions): void
    {
        $this->sharedTransactions = $sharedTransactions;
    }

    /**
     * @return Collection
     */
    public function getSharedWidgets()
    {
        return $this->sharedWidgets;
    }

    /**
     * @param Collection $sharedWidgets
     */
    public function setSharedWidgets($sharedWidgets): void
    {
        $this->sharedWidgets = $sharedWidgets;
    }

    /**
     * @return SharedCustomersServices|null
     */
    public function getSharedCustomersServices(): ?SharedCustomersServices
    {
        return $this->sharedCustomersServices;
    }

    /**
     * @param SharedCustomersServices|null $sharedCustomersServices
     */
    public function setSharedCustomersServices(?SharedCustomersServices $sharedCustomersServices): void
    {
        $this->sharedCustomersServices = $sharedCustomersServices;
    }

    /**
     * @return SharedAmocrm|null
     */
    public function getSharedAmocrm(): ?SharedAmocrm
    {
        return $this->sharedAmocrm;
    }

    /**
     * @param SharedAmocrm|null $sharedAmocrm
     */
    public function setSharedAmocrm(?SharedAmocrm $sharedAmocrm): void
    {
        $this->sharedAmocrm = $sharedAmocrm;
    }

    /**
     * @return SharedCustomers|null
     */
    public function getSharedCustomers(): ?SharedCustomers
    {
        return $this->sharedCustomers;
    }

    /**
     * @param SharedCustomers|null $sharedCustomers
     */
    public function setSharedCustomers(?SharedCustomers $sharedCustomers): void
    {
        $this->sharedCustomers = $sharedCustomers;
    }

    public function jsonSerialize()
    {
        return self::serializing();
    }
}
